name: Build other
on:
  workflow_dispatch:
  workflow_call:
    inputs:
      reference:
        required: false
        type: string
      runner:
        required: true
        type: string
      uploading:
        type: string
    secrets:
      KEY_TORRENTS:
        required: true
      KNOWN_HOSTS_UPLOAD:
        required: true

jobs:  

  gradle:
    if: ${{ github.repository_owner == 'Armbian' && inputs.reference != '' }}
    strategy:
      fail-fast: false
      matrix:
        board: [bananapi]
        release: [focal]
    
    name: Variant
    runs-on: ${{ inputs.runner }}
    steps:

      - name: Clean temporally folders
        run: |
          sudo rm -rf build/.tmp 2>/dev/null || true
          sudo mountpoint -q build/output/debs && sudo fusermount -u build/output/debs || true
          sudo mountpoint -q build/output/debs-beta && sudo fusermount -u build/output/debs-beta || true
          sudo mountpoint -q build/cache/rootfs && sudo fusermount -u build/cache/rootfs || true
          sudo mountpoint -q build/cache/toolchain && sudo fusermount -u build/cache/toolchain || true
          sudo chown -R $USER:$USER build || true

      - name: Checkout Armbian build script
        uses: actions/checkout@v3
        with:
          repository: armbian/build
          path: build
          fetch-depth: '100'
          ref:  ${{ inputs.reference }}
          clean: false

      - name: Checkout support scripts
        uses: actions/checkout@v3
        with:
          fetch-depth: '100'
          repository: armbian/scripts
          path: scripts
          clean: false

      - name: Install SSH key for storage
        if: ${{ inputs.uploading == 'true' }}
        uses: shimataro/ssh-key-action@v2
        with:
          key: ${{ secrets.KEY_TORRENTS }}
          known_hosts: ${{ secrets.KNOWN_HOSTS_UPLOAD }}
          if_key_exists: replace

      - name: Fix permissions
        run: |

          # make sure no temporally dirs are mounted from previous runs
          while :
          do
              sudo pkill compile.sh || true
              sudo pkill arm-binfmt-P || true
              sudo pkill aarch64-binfmt-P || true
              sudo pkill pixz || true
              sudo mountpoint -q build/output/images && sudo fusermount -u build/output/images || true
              sudo mountpoint -q build/output/debs && sudo fusermount -u build/output/debs || true
              sudo mountpoint -q build/output/debs-beta && sudo fusermount -u build/output/debs-beta || true
              [[ "$(df | grep "/.tmp" | wc -l)" -eq 0 && $(sudo mountpoint -q build/output/images; echo $?) -eq 1 ]] && sudo rm -rf build/.tmp && break
              echo "Mounted temp directories. Trying to unmount."
              df | grep ".tmp" | awk '{print $6}' | xargs sudo umount 2>/dev/null || true
              sudo rm -f build/cache/hash/*
              sudo rm -f build/cache/hash-beta/*
              sleep 10
          done
          [[ -d build/.git ]] && sudo chown -R $USER:$USER build/.git || true
          [[ -d build/output/images ]] && sudo rm -rf build/output/images/* || true

      - name: Mount upload folders
        if: ${{ inputs.uploading == 'true' }}
        run: |

          # use this only on our runners
          if [[ ${{ inputs.runner }} != "ubuntu-latest" ]]; then
          # mount deploy target
          sudo apt-get -y -qq install sshfs
          sudo mkdir -p /root/.ssh/
          sudo cp ~/.ssh/known_hosts /root/.ssh/
          sudo mkdir -p build/output/debs || true
          sudo mkdir -p build/output/debs-beta || true
          sudo sshfs upload@users.armbian.com:/debs build/output/debs -o IdentityFile=~/.ssh/id_rsa -o reconnect,nonempty -o allow_other
          sudo sshfs upload@users.armbian.com:/debs-beta build/output/debs-beta -o IdentityFile=~/.ssh/id_rsa -o reconnect,nonempty -o allow_other
          fi

      - name: Sync
        run: |

          sudo mkdir -p build/userpatches build/cache/hash build/cache/hash-beta
          sudo rm -f build/userpatches/targets.conf
          sudo cp scripts/configs/* build/userpatches/
          sudo rm -r build/cache/hash/* build/cache/hash-beta/* 2> /dev/null || true

      - name: Pull Docker image
        run: |

          [[ -z $(command -v docker) ]] && sudo apt-get -yy install docker containerd docker.io
          sudo docker kill $(sudo docker ps -q) 2>/dev/null || true
          sudo docker image rm $(sudo docker images | grep -v $(cat build/VERSION | cut -d"." -f1-2)"-$(dpkg --print-architecture)" | awk 'NR>1 {print $3}') 2> /dev/null || true 
          sudo docker pull ghcr.io/armbian/build:$(cat build/VERSION | cut -d"." -f1-2)"-$(dpkg --print-architecture)"

      - name: Build test image
        run: |

          mkdir -p build/userpatches
          sudo cp scripts/configs/* build/userpatches/
          cd build
          export TERM=dumb
          sed -i "s/-it --rm/-i --rm/" userpatches/config-docker.conf          
          ./compile.sh docker \
          BETA=yes \
          EXPERT=yes \
          BOARD=${{ matrix.board }} \
          BRANCH=current \
          RELEASE=${{ matrix.release }} \
          CLEAN_LEVEL="make,oldcache" \
          BUILD_MINIMAL=no \
          KERNEL_ONLY=yes \
          KERNEL_CONFIGURE=no \
          IGNORE_UPDATES=yes \
          REPOSITORY_INSTALL="u-boot,kernel,armbian-bsp-cli" \
          SKIP_EXTERNAL_TOOLCHAINS=yes \
          USE_MAINLINE_GOOGLE_MIRROR="yes"

      - name: Upload artefacts
        uses: actions/upload-artifact@v2
        with:
          name: firmware-zsh-armbian-config
          path: build/output/debs-beta/*.deb
          if-no-files-found: ignore
          retention-days: 7

      - name: Unmount folders

        run: |

            sudo mountpoint -q build/output/debs && sudo fusermount -u build/output/debs || true
            sudo mountpoint -q build/output/debs-beta && sudo fusermount -u build/output/debs-beta || true
            sudo mountpoint -q build/cache/toolchain && sudo fusermount -u build/cache/toolchain || true
